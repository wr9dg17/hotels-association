// Upload avatar/cover
function uploadPhotoPreview(input, el) {
    if (input.files && input.files[0]) {
        const reader = new FileReader();

        reader.onload = (e) => {
            el.attr("src", e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

var uploadAvatarField = $('.upload-avatar');
var avatarPreview = $('.avatar-preview');

$('.trigger-avatar-upload').click(function () {
    uploadAvatarField.trigger('click');
});

uploadAvatarField.change(function() {
    uploadPhotoPreview(this, avatarPreview);
});

var uploadCoverField = $(".upload-cover");
var coverPreview = $('.cover-preview');

$('.trigger-cover-upload').click(function () {
    uploadCoverField.trigger('click');
});

uploadCoverField.change(function() {
    uploadPhotoPreview(this, coverPreview);
});