const htmlEl = $("html");
const currentColorTheme = localStorage.getItem("colorTheme");
let theme = currentColorTheme;

if (currentColorTheme !== null) {
    htmlEl.attr("data-theme", currentColorTheme);
} else {
    htmlEl.attr("data-theme", "light");
    localStorage.setItem("colorTheme", "light");
    theme = "light";
}

$(".theme-toggler").click(function() {
    theme = theme === "light" ? "dark" : "light";
    htmlEl.attr("data-theme", theme);
    localStorage.setItem("colorTheme", theme);
});