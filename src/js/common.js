// Styles
import "../css/fontello.css";
import "swiper/css/swiper.min.css";
import "air-datepicker/dist/css/datepicker.min.css";
import "aos/dist/aos.css";
import "../css/toastr.css";
import "../scss/style.scss";
// Js
import AOS from "aos";
import Swiper from "swiper";
import "air-datepicker/dist/js/datepicker";
import "./theme";
import az from "./datepicker/datepicker.az";
import en from "air-datepicker/dist/js/i18n/datepicker.en";
import "./bootstrap/bootstrap";
import swal from "sweetalert";

$(".lds-spinner").addClass("visible");

$(window).on("load", () => {
    $(".ng-scope").fadeOut(150);

    // AOS refresh
    AOS.refresh();

    // Page covers animation
    const pageIntro = $(".page-intro");
    
    if (pageIntro.length > 0) {
        pageIntro.addClass("loaded");
    }
});

$(document).ready(() => {
    // AOS init
    AOS.init({
        once: true,
        startEvent: "DOMContentLoaded",
        disable: window.innerWidth < 1024,
    });

    // Toggle menu
    const header = $("header");
    const menuToggler = $(".menu-toggler");

    menuToggler.click(function() {
        header.toggleClass("active");
        $(this).toggleClass("active");
    });

    // Datepicker init
    let dateFrom = $(".date-from");
    let dateTo = $(".date-to");

    dateFrom.datepicker({
        position: "bottom center",
        dateFormat: "dd-mm-yyyy",
        onSelect: function(fd, date, inst) {
            $(inst.el).trigger("change");
            
            dateTo.datepicker({
                minDate: date
            })
        }
    });

    dateTo.datepicker({
        position: "bottom center",
        dateFormat: "dd-mm-yyyy",
        minDate: new Date(),
        onSelect: function(fd, date, inst) {
            $(inst.el).trigger("change");
        }
    });

    // Add/remove background on header
    let isScrolling;
 
    $(window).on("scroll", () => {
        if(isScrolling) {
            window.clearTimeout(isScrolling);
        }

        isScrolling = window.setTimeout(function() {
            handleAddBackground();
        }, 50);
    });

    const handleAddBackground = () => {
        if ( $(window).scrollTop() > ($(window).innerHeight() / 2) ) {
            header.addClass("is-scrolling");
        } else {
            header.removeClass("is-scrolling");
        }
    };

    handleAddBackground();

    // Search modal
    const modalSearch = $("#modal-search");
    const searchForm = $(".search-form");
    const searchField = $(".search-form .form-control");
    const clearSearch = $(".clear-search");

    modalSearch.on("shown.bs.modal", function() {
        searchField.focus();
    });

    modalSearch.on("hide.bs.modal", function() {
        searchField.val("");
    });

    searchField.on("keyup", function() {
        let val = $(this).val().trim();

        setTimeout(function() {
            if (val.length > 3) {
                searchForm.addClass("active");
            } else {
                searchForm.removeClass("active");
            }
        }, 300);
    });

    clearSearch.on("click", function() {
        searchField.val("");
        searchForm.removeClass("active");
    });

    // Intro slider
    const introSlider = new Swiper(".intro-slider .swiper-container", {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        autoplay: {
            delay: 6000
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        effect: "fade"
    });

    // Services
    const hiddenServices = $(".hidden-services");
    const toggleServices = $(".toggle-services");

    toggleServices.click(function() {
        hiddenServices.slideToggle(150);
        $(this).toggleClass("is-visible");
    });

    // Statistics slider
    const x4SliderOptions = {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        spaceBetween: 24,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets'
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                noSwipingClass: "no-swipe"
            },
            768: {
                slidesPerView: 3
            },
            575: {
                slidesPerView: 2
            }
        }
    };

    const statisticsSlider = new Swiper(".statistics-slider", x4SliderOptions);

    // Latest news slider
    const x3SliderOptions = {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        autoplay: {
            delay: 7000
        },
        spaceBetween: 24,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                noSwipingClass: "no-swipe",
            },
            575: {
                slidesPerView: 2
            }
        }
    };

    const newsSlider = new Swiper(".news-slider", x3SliderOptions);

    $(".news .prev-slide").click(() => {
        newsSlider.slidePrev();
    });

    $(".news .next-slide").click(() => {
        newsSlider.slideNext();
    });

    // Events slider
    const eventsSlider = new Swiper(".events-slider", x3SliderOptions);

    $(".events .prev-slide").click(() => {
        eventsSlider.slidePrev();
    });

    $(".events .next-slide").click(() => {
        eventsSlider.slideNext();
    });

    // Partners slider
    const partnersSlider = new Swiper(".partners-slider", {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        spaceBetween: 24,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        on: {
            init: function() {
                let slidesLength = $(".partners-slider .swiper-slide").length;
                let pagination = $(".partners-slider .swiper-pagination");

                if ( slidesLength > 6 ) {
                    this.params.noSwipingClass = "allow-swipe";
                    pagination.removeClass("visible-1024");
                }
            }
        },
        breakpoints: {
            1024: {
                slidesPerView: 6,
                noSwipingClass: "no-swipe",
            },
            900: {
                slidesPerView: 5
            },
            768: {
                slidesPerView: 4
            },
            576: {
                slidesPerView: 3
            },
            320: {
                slidesPerView: 2,
                slidesPerGroup: 2
            }
        }
    });

    // Event/news inner
    const x1SliderOptions = {
        autoplay: {
            delay: 6000
        },
        spaceBetween: 12,
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        }
    };

    const otherEvents = new Swiper(".other-articles .swiper-container", x1SliderOptions);

    // Gallery slider
    const gallerySlider = new Swiper(".gallery", {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        breakpoints: {
            992: {
                slidesPerView: 4,
                spaceBetween: 24
            },
            575: {
                slidesPerView: 3,
                spaceBetween: 24
            },
            300: {
                slidesPerView: 2,
                spaceBetween: 12
            }
        }
    });

    $(".gallery-container .prev-slide").click(() => {
        gallerySlider.slidePrev();
    });

    $(".gallery-container .next-slide").click(() => {
        gallerySlider.slideNext();
    });

    // Toggle password
    $(".toggle-password i").click(function() {
        let input = $(this).closest(".form-group").find("input");

        if (input.attr("type") === "password") {
            input.attr("type", "text");
            $(this).removeClass("icon-eye").addClass("icon-eye-off");
        } else {
            input.attr("type", "password");
            $(this).removeClass("icon-eye-off").addClass("icon-eye");
        }
    });

    // Alerts
    $(".alert .close").on("click", function() {
        $(this).closest(".alert").remove();
    });

    // Sweetalert
    $("a.sweetalert-confirm").click(function(e) {
        e.preventDefault();
        const url = $(this).attr("href");
        
        swal({
            title: "Are you sure?",
            buttons: true
        }).then((confirmed) => {
            if (confirmed) {
                window.location = url;
            }
        })
    });

    // Close cookies popup
    const cookiesAccepted = localStorage.getItem("cookiesAccepted");
    const cookiesPopup = $(".cookies-usage");
    const acceptCookies = $(".accept-cookies");

    if (cookiesAccepted === null) {
        cookiesPopup.show();
    }

    acceptCookies.click(function() {
        cookiesPopup.remove();
        localStorage.setItem("cookiesAccepted", true);
    });

});
