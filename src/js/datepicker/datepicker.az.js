;(function ($) { $.fn.datepicker.language['az'] = {
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    daysMin: ['BE', 'ÇA', 'ÇƏ', 'CA', 'Cü', 'Şə', 'Ba'],
    months: ['Yanvar','Fevral','Mart','Aprel','May','İyun', 'İyul','Avgust','Sentyabr','Oktyabr','Noyabr','Dekabr'],
    monthsShort: ['Yan', 'Fev', 'Mar', 'Apr', 'May', 'İyun', 'İyul', 'Avg', 'Sen', 'Okt', 'Noy', 'Dek'],
    today: 'Bugün',
    clear: 'Sil',
    dateFormat: 'mm/dd/yyyy',
    timeFormat: 'hh:ii aa',
    firstDay: 0
}; })(jQuery);